---
title: here we go
subtitle: new stuff
date: date 2021-09-09T13:26:04+01:00
tags: ["niehus", "isel", "lisboa"]
type: post
---







# 123

# 456

### Intro

et situs vilate inis et avernit.

You can [add external links](https://www.youtube.com/watch?v=dQw4w9WgXcQ) using square brackets here, make font **bold**, *cursive* or ~deleted~. The format supported here includes exteions from [GitHub Flavored Markdown](https://github.github.com/gfm) which you may already be familiar with, footnotes[^1] and pictures:

![Crepe](http://s3-media3.fl.yelpcdn.com/bphoto/cQ1Yoa75m2yUFFbY2xwuqw/348s.jpg)

And of course source code:

```javascript
console.log('hello world');
```

- And
- lists

[^1]: Strictly speaking this isn't standard Markdown either.




$$
-\frac{\partial}{\partial z} v(z,t) = R'~i(z,t) + L'~\frac{\partial}{\partial t} i(z,t)
$$
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mniehus%2Fgdstest/main?filepath=gds_tutorial2)